<h3>Croatian</h3>
<ul>
    <li><a href="mailto:mail@milotype.de">Milo Ivir</a></li>
</ul>

<h3>Czech</h3>
<ul>
    <li><a href="mailto:jiri.doubravsky@gmail.com">Jiří Doubravský</a></li>
</ul>

<h3>Dutch</h3>
<ul>
    <li><a href="mailto:vistausss@outlook.com">Heimen Stoffels</a></li>
</ul>

<h3>French</h3>
<ul>
    <li><a href="mailto:31b32@tutanota.com">Gereko</a></li>
    <li><a href="mailto:anarchistcat@riseup.net">anarchistcat</a></li>
    <li><a href="mailto:epost@anotheragency.no">Allan Nordhøy</a></li>
    <li><a href="mailto:nico_bidault@yahoo.fr">Nicolas Bidault</a></li>
</ul>

<h3>German</h3>
<ul>
    <li><a href="mailto:johanneskeyser@posteo.de">Johannes Keyser</a></li>
    <li><a href="mailto:mail.ka@mailbox.org">nautilusx</a></li>
    <li><a href="mailto:mail@milotype.de">Milo Ivir</a></li>
</ul>

<h3>Indonesian</h3>
<ul>
    <li><a href="mailto:ditokpl@gmail.com">ditokp</a></li>
</ul>

<h3>Italian</h3>
<ul>
    <li><a href="mailto:marcoscarpetta02@gmail.com">Marco Scarpetta</a></li>
</ul>

<h3>Japanese</h3>
<ul>
    <li><a href="mailto:naofum@gmail.com">naofum</a></li>
    <li><a href="mailto:yamada_strong_yamada_nice_64bit@yahoo.co.jp">YAMADA Shinichirou</a></li>
</ul>

<h3>Norwegian Bokmål</h3>
<ul>
    <li><a href="mailto:epost@anotheragency.no">Allan Nordhøy</a></li>
</ul>

<h3>Polish</h3>
<ul>
    <li><a href="mailto:blade-14@o2.pl">Michal Biesiada</a></li>
</ul>

<h3>Portuguese</h3>
<ul>
    <li><a href="mailto:so.boston.android@gmail.com">Sergio Marques</a></li>
    <li><a href="mailto:ssantos@web.de">ssantos</a></li>
</ul>

<h3>Portuguese (Brazil)</h3>
<ul>
    <li>Fúlvio Alves</li>
    <li><a href="mailto:barbaratostes@gmail.com">Barbara Tostes</a></li>
</ul>

<h3>Russian</h3>
<ul>
    <li><a href="mailto:abomze@mail.ru">Adolph Bomze</a></li>
    <li><a href="mailto:denis.a.shilin@gmail.com">Denis Shilin</a></li>
</ul>

<h3>Spanish</h3>
<ul>
    <li><a href="mailto:mgfuentesl@outlook.com">M. G. Fuentes</a></li>
    <li><a href="mailto:fitojb@ubuntu.com">Adolfo Jayme Barrientos</a></li>
    <li><a href="https://gitlab.com/xoan">Xoán Sampaíño</a></li>
</ul>

<h3>Swedish</h3>
<ul>
    <li><a href="mailto:eson@svenskasprakfiler.se">Åke Engelbrektson</a></li>
</ul>

<h3>Turkish</h3>
<ul>
    <li><a href="mailto:makcan@gmail.com">Mesut Akcan</a></li>
    <li><a href="mailto:libreajans@gmail.com">Sabri Ünal</a></li>
</ul>
