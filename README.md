# ![PDF Mix Tool logo](https://scarpetta.eu/pdfmixtool/icon.svg) PDF Mix Tool

<a href="https://build.snapcraft.io/user/marcoscarpetta/pdfmixtool">
<img src="https://build.snapcraft.io/badge/marcoscarpetta/pdfmixtool.svg" alt="Snap status" /></a>
<a href="https://hosted.weblate.org/engage/pdf-mix-tool/?utm_source=widget">
<img src="https://hosted.weblate.org/widgets/pdf-mix-tool/-/svg-badge.svg" alt="Translations status" /></a>

<p>
    PDF Mix Tool is a simple and lightweight application that allows you to
    perform common editing operations on PDF files.
</p>

![Screenshot](https://scarpetta.eu/pdfmixtool/merge_files.png)

<p>Base operations it can perform are the following:</p>
<ul>
    <li>Merge two or more files specifying a page set for each of them</li>
    <li>Rotate pages</li>
    <li>Composite more pages onto a single one (N-up)</li>
    <li>Combinations of all of the above</li>
</ul>
<p>
    Besides, it can also mix files alternating their pages, generate booklets,
    add white pages to a PDF file, delete pages from a PDF file, extract pages
    from a PDF file. 
</p>

## Useful links
[Changelog/Roadmap](CHANGELOG.md)

[Website](https://scarpetta.eu/pdfmixtool/): here you can find usage
guides, building instructions and all other information.
