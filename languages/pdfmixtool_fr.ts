<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>À propos de PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="71"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="78"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="68"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>Une application pour effectuer des opérations d&apos;édition courantes sur des fichiers PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Traducteurs</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Envoyer une demande de fusion de branches</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Signaler un bug</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Aider à traduire</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Contribuer</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Notes de version</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="36"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="64"/>
        <source>Overwrite File?</source>
        <translation>Remplacer le fichier&#x202f;?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="65"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>Un fichier nommé «&#x202f;%1&#x202f;» existe déjà. Voulez-vous le remplacer&#xa0;?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="71"/>
        <source>Always overwrite</source>
        <translation>Toujours remplacer</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="89"/>
        <source>Save PDF file</source>
        <translation>Enregistrer le fichier PDF</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="93"/>
        <source>PDF files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="34"/>
        <source>Add empty pages</source>
        <translation>Ajouter des pages vierges</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="46"/>
        <source>Count:</source>
        <translation>Nombre&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Page size</source>
        <translation>Taille de la page</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="53"/>
        <source>Same as document</source>
        <translation>Identique au document</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="56"/>
        <source>Custom:</source>
        <translation>Personnalisée&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="81"/>
        <source>Standard:</source>
        <translation>Standard&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="90"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Landscape</source>
        <translation>Paysage</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="95"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="98"/>
        <source>Before</source>
        <translation>Avant</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="100"/>
        <source>After</source>
        <translation>Après</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="103"/>
        <source>Page:</source>
        <translation>Page&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="117"/>
        <source>Save as…</source>
        <translation>Enregistrer sous…</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="33"/>
        <source>Booklet</source>
        <translation>Livret</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="42"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="43"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="44"/>
        <source>Binding:</source>
        <translation>Reliure&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="45"/>
        <source>Use last page as back cover:</source>
        <translation>Utilisez la dernière page comme couverture arrière&#x202f;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="52"/>
        <source>Generate booklet</source>
        <translation>Générer la brochure</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="68"/>
        <source>Save booklet PDF file</source>
        <translation>Enregistrer le livret PDF</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="72"/>
        <source>PDF files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="34"/>
        <source>Delete pages</source>
        <translation>Supprimer les pages</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="54"/>
        <source>Save as…</source>
        <translation>Enregistrer sous…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Modifier le profil multipage</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="39"/>
        <source>Standard size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="40"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="41"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Centrer</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Taille de la page de sortie</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Taille personnalisée :</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Mise en page</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Lignes:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Colonnes:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Rotation:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Écartement:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Alignement des pages</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Vertical:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="34"/>
        <source>Edit page layout</source>
        <translation>Modifier la mise en page</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="55"/>
        <source>No rotation</source>
        <translation>Sans rotation</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="62"/>
        <source>Rotation:</source>
        <translation>Rotation&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="64"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="124"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="211"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="136"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="219"/>
        <source>New custom profile…</source>
        <translation>Nouveau profil personnalisé…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="78"/>
        <source>Multipage:</source>
        <translation>Multipage&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="86"/>
        <source>Scale page:</source>
        <translation>Échelle de la page&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="94"/>
        <source>Save as…</source>
        <translation>Enregistrer sous…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Modifier les propriétés des fichiers PDF</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Sans rotation</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Multipage:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Rotation:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Output PDF base name:</source>
        <translation>Nom du PDF de sortie&#xa0;:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="49"/>
        <source>Extract to individual PDF files</source>
        <translation>Extraire dans des fichiers PDF individuels</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="37"/>
        <source>Extract pages</source>
        <translation>Extraire les pages</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="62"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="84"/>
        <source>Extract…</source>
        <translation>Extrait…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="79"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="190"/>
        <source>Extract to single PDF</source>
        <translation>Extraire dans un PDF unique</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="114"/>
        <source>Select save directory</source>
        <translation>Sélectionnez le répertoire d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="194"/>
        <source>PDF files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation>Ordre des pages&#x202f;:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Pages :</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Multipage:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Rotation :</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>Entrée du plan&#xa0;:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation>Inverser l&apos;ordre des pages&#x202f;:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>Nouveau profil personnalisé…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Sans rotation</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Pages :</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Multipage:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Rotation:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>Entrer le contour :</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>Add PDF file</source>
        <translation>Ajouter un fichier pdf :</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>Remove file</source>
        <translation>Effacer le fichier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Generate PDF</source>
        <translation>Générer un PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Select the JSON file containing the files list</source>
        <translation>Sélectionnez le fichier JSON contenant la liste des fichiers</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="853"/>
        <source>PDF generation error</source>
        <translation>Erreur de génération de PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="180"/>
        <source>Select one or more PDF files to open</source>
        <translation>Sélectionnez un ou plusieurs fichiers PDF à ouvrir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="165"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>Main toolbar</source>
        <translation>Barre d&apos;outils principale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="95"/>
        <source>Multiple files</source>
        <translation>Plusieurs fichiers</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="96"/>
        <source>Single file</source>
        <translation>Fichier unique</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="123"/>
        <source>Multipage profiles…</source>
        <translation>Profils multipages…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="133"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="208"/>
        <source>Load files list</source>
        <translation>Charger la liste des fichiers</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="213"/>
        <source>Save files list</source>
        <translation>Enregistrer la liste des fichiers</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Open PDF file…</source>
        <translation>Ouvrir un fichier PDF…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <location filename="../src/mainwindow.cpp" line="952"/>
        <source>PDF files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="48"/>
        <source>Alternate mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="475"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>JSON files (*.json)</source>
        <translation>Fichiers JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>Error while reading the JSON file!</source>
        <translation>Erreur lors de la lecture du fichier JSON&#x202f;!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="482"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>Une erreur s&apos;est produite lors de la lecture du fichier JSON&#x202f;!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="489"/>
        <source>Select a JSON file</source>
        <translation>Sélectionnez un fichier JSON</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <location filename="../src/mainwindow.cpp" line="971"/>
        <source>Error opening file</source>
        <translation>Erreur lors de l&apos;ouverture du fichier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>Output pages: %1</source>
        <translation>Pages de sortie : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>Select a PDF file</source>
        <translation>Sélectionner un fichier PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Files saved in %1.</source>
        <translation>Fichiers enregistrés dans %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>File %1 saved.</source>
        <translation>Fichier %1 enregistré.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="836"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt; Les pages de sortie du fichier &lt;b&gt;%1 &lt;/b&gt; sont mal formatées. Veuillez vous assurer que vous avez respecté les règles suivantes&#x202f;: &lt;/p&gt; &lt;ul&gt; &lt;li&gt; les intervalles de pages doivent être écrits en indiquant la première page et la dernière page séparées par un tiret (par exemple &quot;1-5&quot;)&#x202f;; &lt;/li &gt;&lt;li&gt; les pages simples et les intervalles de pages doivent être séparés par des espaces, des virgules ou les deux (par exemple &quot;1, 2, 3, 5-10&quot; ou &quot;1 2 3 5-10&quot;)&#x202f;; &lt;/li&gt; &lt;li&gt; toutes les pages et tous les intervalles de pages doivent être compris entre 1 et le nombre de pages du fichier PDF&#x202f;; &lt;/li&gt; &lt;li&gt; seuls les nombres, les espaces, les virgules et les tirets peuvent être utilisés. Tous les autres caractères ne sont pas autorisés. &lt;/li&gt; &lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="860"/>
        <source>Save PDF file</source>
        <translation>Enregistrer le fichier PDF</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Nouveau profile…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>Supprimer le profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="43"/>
        <source>Manage multipage profiles</source>
        <translation>Gérer les profils multipages</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="76"/>
        <source>Edit profile</source>
        <translation>Modifier le profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="122"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="124"/>
        <source>Custom profile</source>
        <translation>Profil personnalisé</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="183"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="206"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="184"/>
        <source>Profile name can not be empty.</source>
        <translation>Le nom du profil ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="193"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="207"/>
        <source>Profile name already exists.</source>
        <translation>Le nom du profil existe déjà.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="44"/>
        <source>Pages:</source>
        <translation type="unfinished">Pages :</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="45"/>
        <source>Even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>All pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="108"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="124"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>portrait</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>paysage</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n page</numerusform>
            <numerusform>%n pages</numerusform>
        </translation>
    </message>
</context>
</TS>
