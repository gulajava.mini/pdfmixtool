<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Acerca de PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Versión %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="71"/>
        <source>Website</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="78"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="68"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>Una aplicación para efectuar modificaciones comunes en archivos PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Traductores</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Enviar una solicitud de incorporación</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Reportar un error</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Ayudar con la traducción</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Contribuir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Registro de cambios</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="36"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="64"/>
        <source>Overwrite File?</source>
        <translation>¿Quiere sobrescribir el archivo?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="65"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>Ya existe un archivo denominado «%1». ¿Quiere sobrescribirlo?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="71"/>
        <source>Always overwrite</source>
        <translation>Sobrescribir siempre</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="89"/>
        <source>Save PDF file</source>
        <translation>Guardar archivo PDF</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="93"/>
        <source>PDF files (*.pdf)</source>
        <translation>Archivos PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="34"/>
        <source>Add empty pages</source>
        <translation>Añadir páginas vacías</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="46"/>
        <source>Count:</source>
        <translation>Recuento:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Page size</source>
        <translation>Tamaño de página</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="53"/>
        <source>Same as document</source>
        <translation>Igual que en el documento</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="56"/>
        <source>Custom:</source>
        <translation>Personalizado:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="81"/>
        <source>Standard:</source>
        <translation>Estándar:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="90"/>
        <source>Portrait</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Landscape</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="95"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="98"/>
        <source>Before</source>
        <translation>Antes</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="100"/>
        <source>After</source>
        <translation>Después</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="103"/>
        <source>Page:</source>
        <translation>Página:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="117"/>
        <source>Save as…</source>
        <translation>Guardar como…</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="33"/>
        <source>Booklet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="42"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="43"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="44"/>
        <source>Binding:</source>
        <translation>Encuadernación:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="45"/>
        <source>Use last page as back cover:</source>
        <translation>Utilizar última página como contracubierta:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="52"/>
        <source>Generate booklet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="68"/>
        <source>Save booklet PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="72"/>
        <source>PDF files (*.pdf)</source>
        <translation>Archivos PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="34"/>
        <source>Delete pages</source>
        <translation>Eliminar páginas</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="54"/>
        <source>Save as…</source>
        <translation>Guardar como…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Editar perfil de página múltiple</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="39"/>
        <source>Standard size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="40"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="41"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Tamaño de la página de salida</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Tamaño personalizado:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Disposición de páginas</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Filas:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Columnas:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Giro:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Espaciado:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Alineación de página</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Vertical:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Márgenes</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="34"/>
        <source>Edit page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="55"/>
        <source>No rotation</source>
        <translation>Sin giro</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="62"/>
        <source>Rotation:</source>
        <translation>Giro:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="64"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="124"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="211"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="136"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="219"/>
        <source>New custom profile…</source>
        <translation>Perfil personalizado nuevo…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="78"/>
        <source>Multipage:</source>
        <translation>Página múltiple:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="86"/>
        <source>Scale page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="94"/>
        <source>Save as…</source>
        <translation type="unfinished">Guardar como…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Editar propiedades de los archivos PDF</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Sin giro</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Página múltiple:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Giro:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Output PDF base name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="49"/>
        <source>Extract to individual PDF files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="37"/>
        <source>Extract pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="62"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="84"/>
        <source>Extract…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="79"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="190"/>
        <source>Extract to single PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="114"/>
        <source>Select save directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="194"/>
        <source>PDF files (*.pdf)</source>
        <translation>Archivos PDF (*.pdf)</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Página:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Página múltiple:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Giro:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>Entrada de esquema:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>Perfil personalizado nuevo…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Sin giro</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Páginas:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Página múltiple:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Giro:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>Entrada de esquema:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>Add PDF file</source>
        <translation>Añadir archivo PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>Move up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Move down</source>
        <translation>Bajar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>Remove file</source>
        <translation>Quitar archivo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Generate PDF</source>
        <translation>Generar PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Select the JSON file containing the files list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="853"/>
        <source>PDF generation error</source>
        <translation>Generación de error en PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="180"/>
        <source>Select one or more PDF files to open</source>
        <translation>Seleccione uno o más archivos PDF para abrir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="165"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>Main toolbar</source>
        <translation>Barra de herramientas principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Menu</source>
        <translation>Menú</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="95"/>
        <source>Multiple files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="96"/>
        <source>Single file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="123"/>
        <source>Multipage profiles…</source>
        <translation>Perfiles de página múltiple…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="133"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="208"/>
        <source>Load files list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="213"/>
        <source>Save files list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Open PDF file…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <location filename="../src/mainwindow.cpp" line="952"/>
        <source>PDF files (*.pdf)</source>
        <translation>Archivos PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="48"/>
        <source>Alternate mix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="475"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>JSON files (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>Error while reading the JSON file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="482"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="489"/>
        <source>Select a JSON file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <location filename="../src/mainwindow.cpp" line="971"/>
        <source>Error opening file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>Output pages: %1</source>
        <translation>Páginas de salida: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>Select a PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Files saved in %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>File %1 saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="836"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Las páginas de salida del archivo &lt;b&gt;%1&lt;/b&gt; tienen un formato incorrecto. Cerciórese de que ha cumplido las reglas siguientes:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;los intervalos de páginas deben escribirse indicando las páginas primera y última separadas por un guion (p.&#xa0;ej., «1-5»);&lt;/li&gt;&lt;li&gt;las páginas sencillas y los intervalos de páginas deben separarse por espacios, comas o ambos signos (p.&#xa0;ej., «1, 2, 3, 5-10» o «1 2 3 5-10»);&lt;/li&gt;&lt;li&gt;todas las páginas e intervalos de páginas deben figurar entre 1 y el total de páginas del archivo PDF;&lt;/li&gt;&lt;li&gt;solo pueden utilizarse números, espacios, comas y guiones. No se permite ningún otro carácter.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="860"/>
        <source>Save PDF file</source>
        <translation>Guardar archivo PDF</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Perfil nuevo…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>Eliminar perfil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="43"/>
        <source>Manage multipage profiles</source>
        <translation>Gestionar perfiles de página múltiple</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="76"/>
        <source>Edit profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="122"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="124"/>
        <source>Custom profile</source>
        <translation>Perfil personalizado</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="183"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="206"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="184"/>
        <source>Profile name can not be empty.</source>
        <translation>El nombre del perfil no puede quedar vacío.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="193"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="207"/>
        <source>Profile name already exists.</source>
        <translation>El nombre del perfil ya existe.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="44"/>
        <source>Pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="45"/>
        <source>Even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>All pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="108"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="124"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>vertical</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>horizontal</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n página</numerusform>
            <numerusform>%n páginas</numerusform>
        </translation>
    </message>
</context>
</TS>
